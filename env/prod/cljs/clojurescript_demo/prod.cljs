(ns clojurescript-demo.prod
  (:require [clojurescript-demo.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
